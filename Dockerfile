FROM ubuntu/apache2:2.4-22.04_beta
ENV TZ=UTC

ARG UID=1000
ARG GID=1000
# Change www-data to match current user's UID/GID to avoid problems with mismatching permissions
RUN export OLD_GID=$(id -g www-data); \
    export OLD_UID=$(id -u www-data); \
    usermod -u $UID www-data; \
    groupmod -g $GID www-data; \
    find / -user $OLD_UID -exec chown -h $UID {} \; ; \
    find / -group $OLD_GID -exec chgrp -h $GID {} \; ; \
    usermod -g $GID www-data

RUN apt update
# Install SVN
RUN apt install -y \
    build-essential \
    git \
    golang \
    libapache2-mod-svn \
    rsync \
    subversion \
    sudo \
    zip

RUN mkdir -p /data/svn/repos
RUN chown www-data:www-data /data/svn/repos
RUN chmod 2775 /data/svn/repos

RUN a2enmod deflate dav dav_svn authz_svn headers proxy proxy_http

RUN mkdir /etc/apache2/svn
RUN chown www-data:www-data /etc/apache2/svn
RUN chmod 0775 /etc/apache2/svn

RUN htpasswd -Bb -c /etc/apache2/api-htpasswd admin ''

RUN chown root:www-data /etc/apache2/api-htpasswd
RUN chmod 0640 /etc/apache2/api-htpasswd
COPY docker/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY docker/www-data-sudo /etc/sudoers.d/

# Download go dependencies
RUN go install github.com/golang/mock/mockgen@v1.6.0
# Make GOPATH writable by www-data
RUN chown www-data:www-data -R /var/www
WORKDIR /opt/svn-manager
