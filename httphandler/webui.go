package httphandler

import (
	"fmt"
	"html/template"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"github.com/armadillica/svn-manager/svnman"
)

// WebUI serves HTTP requests and shows a web UI.
type WebUI struct {
	root               string
	applicationVersion string
	svn                svnman.Manager
}

// TemplateData is the mapping type we use to pass data to the template engine.
type TemplateData map[string]interface{}

// CreateWebUI creates a new HTTP request handler that's bound to the given SVN Man.
func CreateWebUI(webroot, applicationVersion string, svn svnman.Manager) *WebUI {
	log.WithField("webroot", webroot).Info("creating web UI")
	return &WebUI{webroot, applicationVersion, svn}
}

func noDirListing(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/") {
			http.NotFound(w, r)
			return
		}
		h.ServeHTTP(w, r)
	})
}

// Merges 'two' into 'one'
func merge(one map[string]interface{}, two map[string]interface{}) {
	for key := range two {
		one[key] = two[key]
	}
}

// AddRoutes adds the web UI endpoints to the router.
func (web *WebUI) AddRoutes(r *mux.Router) {
	r.HandleFunc("/", web.index).Methods("GET")
	r.HandleFunc("/manage/", web.manage).Methods("GET")
	r.HandleFunc("/manage/repo/{repo-id}/", web.repo).Methods("GET")

	dirname := filepath.Join(web.root, "static")
	static := noDirListing(http.StripPrefix("/static/", http.FileServer(http.Dir(dirname))))
	r.PathPrefix("/static/").Handler(static).Methods("GET")
}

func (web *WebUI) showTemplate(templfname string, w http.ResponseWriter, r *http.Request, data map[string]interface{}) {
	files := []string{
		filepath.Join(web.root, "/templates/", templfname),
		filepath.Join(web.root, "/templates/base.html"),
		filepath.Join(web.root, "/templates/modals/change_access.html"),
		filepath.Join(web.root, "/templates/modals/create_repository.html"),
		filepath.Join(web.root, "/templates/modals/delete_repository.html"),
		filepath.Join(web.root, "/templates/modals/grant_access.html"),
		filepath.Join(web.root, "/templates/modals/revoke_access.html"),
	}
	tmpl, err := template.ParseFiles(files...)
	if err != nil {
		_, logger := logFieldsForRequest(r)
		logger.WithError(err).WithFields(log.Fields{
			"template": templfname,
			"webroot":  web.root,
		}).Error("error parsing HTML template")
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	if err := tmpl.Execute(w, data); err != nil {
		_, logger := logFieldsForRequest(r)
		logger.WithError(err).WithFields(log.Fields{
			"template": templfname,
			"webroot":  web.root,
		}).Error("error executing HTML template")
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
}

func (web *WebUI) index(w http.ResponseWriter, r *http.Request) {
	web.showTemplate("index.html", w, r, nil)
}

func (web *WebUI) manage(w http.ResponseWriter, r *http.Request) {
	repos, err := web.svn.ListRepos()
	if err != nil {
		_, logger := logFieldsForRequest(r)
		logger.WithError(err).WithFields(log.Fields{
			"template": "manage.html",
			"webroot":  web.root,
		}).Error("error listing existing repos")
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}
	data := map[string]interface{}{
		"Version": web.applicationVersion,
		"Repos":   repos,
	}
	web.showTemplate("manage.html", w, r, data)
}

func (web *WebUI) repo(w http.ResponseWriter, r *http.Request) {
	logFields, logger := logFieldsForRequest(r)
	repoID := getRepoID(w, r, logFields)
	if repoID == "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	logger = logger.WithFields(log.Fields{
		"repo_id":  repoID,
		"template": "repo.html",
		"webroot":  web.root,
	})
	info, err := web.svn.GetRepoInfo(repoID)
	if err != nil {
		logger.WithError(err).Error("error reading info file")
	}
	if err == svnman.ErrNotFound {
		logger.Warning("nonexistent repository requested")
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "nonexistent repository requested")
		return
	}
	names, err := web.svn.GetUsernames(repoID)
	if err != nil {
		logger.WithError(err).Error("error reading users")
	}

	data := map[string]interface{}{
		"Version": web.applicationVersion,
		"RepoID":  repoID,
		"Info":    info,
		"Names":   names,
	}
	web.showTemplate("repo.html", w, r, data)
}
