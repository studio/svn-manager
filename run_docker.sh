#!/bin/bash
set -x

export WORKDIR="$(pwd)"
export LOCAL_PORT="${LOCAL_PORT:=8080}"

docker stop svn-manager
docker rm svn-manager

# Pass current user UID and GID: Dockerfile changes www-data to match current user'd IDs.
docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -t svn-manager .

docker run -d -p ${LOCAL_PORT}:80 \
    --name svn-manager \
    -v ${WORKDIR}/docker/etc/apache2/svn:/etc/apache2/svn \
    -v ${WORKDIR}/docker/data/svn/repos:/data/svn/repos \
    -v ${WORKDIR}/ui/static:/var/www/html/static \
    -v ${WORKDIR}:/opt/svn-manager \
    svn-manager

# to get repo configuration files to test with, the following can be used:
#   rsync -r svn.blender.cloud:/etc/apache2/svn docker/etc/apache2
#   rsync -r  --include="*/" --include="info.yaml" --exclude="*" svn.blender.cloud:/data/svn/repos docker/data/svn

docker exec -it svn-manager chown www-data:www-data -R /data/svn
docker exec -it svn-manager chown www-data:www-data -R /etc/apache2/svn

docker exec --user www-data:www-data -it svn-manager go run main.go -verbose -repo /data/svn/repos
