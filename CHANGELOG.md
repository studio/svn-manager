# SVN Manager Changelog

# Version 1.1 (2020-09-15)

- New procedure for building & packaging.
- Documented installation & testing.
- Removed RabbitMQ requirement. It wasn't used at all.


# Version 1.0 (2017-12-22)

- First version used on Blender Cloud.
