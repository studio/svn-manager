OUT := svn-manager
PKG := github.com/armadillica/svn-manager
VERSION := $(shell git describe --tags --dirty --always)
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
STATIC_OUT := ${OUT}-${VERSION}
PACKAGE_PATH := dist/${OUT}-${VERSION}
BUILDOPTS :=

SHA256_CMD = sha256sum
ifeq (, $(shell which $(SHA256_CMD)))
	SHA256_CMD = shasum -a 256
endif

server:
	go build -v -o ${OUT} -ldflags="-X main.applicationVersion=${VERSION}" ${BUILDOPTS} ${PKG}

static: vet lint
	go build -v -o ${STATIC_OUT} -tags netgo -ldflags="-extldflags \"-static\" -w -s -X main.applicationVersion=${VERSION}" ${BUILDOPTS} ${PKG}

run: server
	./${OUT}

version:
	@echo "Package: ${PKG}"
	@echo "Version: ${VERSION}"
	@echo "Packaging to: ${PACKAGE_PATH}"

test:
	go test ${PKG_LIST}

vet: mocks
	@go vet ${PKG_LIST}

lint:
	@for file in ${GO_FILES} ;  do \
		golint $$file ; \
	done

clean:
	rm -fv */*_mock.go
	@go clean -i -x
	rm -f ${OUT}-static-*

mocks: go-get svnman/svnman_mock.go

go-get:
	go get github.com/golang/mock/mockgen

%_mock.go: %.go
	@# notdir-realpath-dir takes the last directory component.
	mockgen -package $(notdir $(realpath $(dir $<))) -source $< -destination $@



package:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_linux
	@$(MAKE) _package_windows
	@$(MAKE) _package_darwin
	@$(MAKE) _package_freebsd
	@$(MAKE) _finish_package

package_linux:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_linux
	@$(MAKE) _finish_package

package_windows:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_windows
	@$(MAKE) _finish_package

package_darwin:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_darwin
	@$(MAKE) _finish_package

package_freebsd:
	@$(MAKE) _prepare_package
	@$(MAKE) _package_freebsd
	@$(MAKE) _finish_package

_package_linux:
	cp svn-manager.service ${PACKAGE_PATH}/
	@$(MAKE) --no-print-directory GOOS=linux GOARCH=amd64 STATIC_OUT=${PACKAGE_PATH}/${OUT} _package_tar
	rm -f ${PACKAGE_PATH}/svn-manager.service

_package_windows:
	@$(MAKE) --no-print-directory GOOS=windows GOARCH=amd64 STATIC_OUT=${PACKAGE_PATH}/${OUT}.exe _package_zip

_package_darwin:
	@$(MAKE) --no-print-directory GOOS=darwin GOARCH=amd64 STATIC_OUT=${PACKAGE_PATH}/${OUT} _package_zip

_package_freebsd:
	@$(MAKE) --no-print-directory GOOS=freebsd GOARCH=amd64 STATIC_OUT=${PACKAGE_PATH}/${OUT} _package_tar

_prepare_package:
	rm -rf ${PACKAGE_PATH}
	mkdir -p ${PACKAGE_PATH}
	rsync ui json_schemas ${PACKAGE_PATH}/ -a --delete-after
	rsync -ua README.md LICENSE.txt ${PACKAGE_PATH}/

_finish_package:
	rm -r ${PACKAGE_PATH}
	rm -f ${PACKAGE_PATH}.sha256
	$(SHA256_CMD) ${PACKAGE_PATH}* | tee ${PACKAGE_PATH}.sha256

_package_tar: static
	tar -C $(dir ${PACKAGE_PATH}) -zcf $(PWD)/${PACKAGE_PATH}-${GOOS}.tar.gz $(notdir ${PACKAGE_PATH})
	rm ${STATIC_OUT}

_package_zip: static
	cd $(dir ${PACKAGE_PATH}) && zip -9 -r -q $(notdir ${PACKAGE_PATH})-${GOOS}.zip $(notdir ${PACKAGE_PATH})
	rm ${STATIC_OUT}


.PHONY: run server version static vet lint clean go-get
