document.addEventListener('DOMContentLoaded', function(){
  openModal = function (modalID) {
    return function() {
      document.getElementById('modal-backdrop').style.display = 'block';
      document.getElementById(modalID).style.display = 'block';
      document.getElementById(modalID).classList.add('show');
    };
  };

  closeModal = function (modalID) {
    return function() {
      document.getElementById('modal-backdrop').style.display = 'none';
      document.getElementById(modalID).style.display = 'none';
      document.getElementById(modalID).classList.remove('show');
    };
  };

  Array.prototype.forEach.call(document.getElementsByClassName('modal-open'), function(el) {
    el.addEventListener('click', openModal(el.dataset.modalId));
  });

  Array.prototype.forEach.call(document.getElementsByClassName('modal-close'), function(el) {
    el.addEventListener('click', closeModal(el.dataset.modalId));
  });
});

function clearErrors(parentEl) {
  const errorsEl = parentEl.getElementsByClassName('errors')[0];
  errorsEl.textContent = '';
  errorsEl.classList.add('d-none');
}

function showErrors(parentEl, errorsText) {
  const errorsEl = parentEl.getElementsByClassName('errors')[0];
  errorsEl.textContent = errorsText;
  errorsEl.classList.remove('d-none');
}

function handleErrors(parentEl, result, message) {
  if (result.status >= 400 && result.status < 500) {
    showErrors(parentEl, `${message}: "${result.statusText}"`);
  }
}

function clearValidity(el) {
  el.setCustomValidity('');
  el.reportValidity();
  el.reportValidity();
}
