document.addEventListener('DOMContentLoaded', function(){
  document.getElementById('createRepositoryForm').addEventListener('submit', function (event) {
    event.preventDefault();

    clearErrors(event.target);

    let data = {};
    Array.prototype.forEach.call(event.target.getElementsByTagName('input'), function(inputEl) {
      data[inputEl.name] = inputEl.value.trim(' ');
    });
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    const baseErrorText = 'Unable to create a repository';
    fetch('/api/repo', {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(data)
    })
      .then(result => {
        if (result.status === 201) {
          window.location.reload();
        } else {
          handleErrors(event.target, result, baseErrorText);
        }
      })
      .catch(error => {
        // console.error(baseErrorText, error);
        showErrors(event.target, `${baseErrorText}: "${error}"`);
      });
  });
});
